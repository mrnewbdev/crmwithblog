## My intentions
Hi, im Manuel, i want to use Laravel in business, so i started to make my own web-app. 
Actually im a trainee so dont judge me about my code :) 
But im allways open for critics.

## My goals
I want to be a Laravel master :). 
Jokes aside. 
In this project i want to realize a full blog system with a seperate login section for admin, author and user.
I also want to implement a comment & rating section for the posts. 

## Status update
02/08/2019 - Started project, setup basic structure of views and routes
